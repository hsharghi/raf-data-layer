error sample model 
```
{
  "error": {
    "message": "Registration failed",
    "code" : 
    "fields": {
      "email": [
        "Invalid email address"
      ],
      "password": [
        "Password should be between 6 and 10 characters",
        "Password should contain both small and capital letters",
      ]
    }
  }
}
```