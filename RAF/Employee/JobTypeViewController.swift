//
//  JobTypeViewController.swift
//  RAF
//
//  Created by Hadi Sharghi on 3/31/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import UIKit

class JobTypeViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var fullTimeButton: UIButton!
    @IBOutlet weak var partTimeButton: UIButton!
    @IBOutlet weak var freelanceButton: UIButton!
    @IBOutlet weak var internshipButton: UIButton!
    @IBOutlet weak var volunteerButton: UIButton!

    enum JobTypes : Int {
        case fullTime = 1
        case partTime = 2
        case freelance = 3
        case internship = 4
        case volunteer = 5
    }
    
    var jobTypes : [JobTypes] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        doneButton.flip()
        
        fullTimeButton.rounded()
        partTimeButton.rounded()
        freelanceButton.rounded()
        internshipButton.rounded()
        volunteerButton.rounded()
        
        [1,2,3,4,5].forEach { tag in
            if let button = self.view.viewWithTag(tag) as? UIButton {
                button.rounded()
            }
        }
        AppData.addGradientLayer(to: self.view)
    
    }

    @IBAction func jobTypeTapped(_ sender: Any) {
        if let button = sender as? UIButton {
            let tag = button.tag
            if let jobType = jobTypes.filter({ $0.rawValue == tag }).first {
                button.toggle(state: .deSelected)
                jobTypes.remove(at: (jobTypes.index(of: jobType)!))
            } else {
                button.toggle(state: .selected)
                jobTypes.append(JobTypes(rawValue: tag)!)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIButton {
    enum ButtonSelectState {
        case selected
        case deSelected
    }
    
    func toggle(state: ButtonSelectState) {
        switch state {
        case .selected:
            self.backgroundColor = AppData.darkGreenColor
        case .deSelected:
            self.backgroundColor = .clear
        }
    }
}
