//
//  CategoryViewController.swift
//  RAF
//
//  Created by Hadi Sharghi on 3/31/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    enum CategoryViewMode {
        case category
        case subCategory
    }
    
    var mode: CategoryViewMode = .category
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()

        nextButton.flip()
        
        AppData.addGradientLayer(to: self.view)

    }

    func setupView() {
        switch mode {
        case .category:
                infoLabel.isHidden = false
                titleLabel.text = "Select the industry you would like to work in"
        case .subCategory:
                infoLabel.isHidden = true
                titleLabel.text = "Select the employment types that you are interested in"
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if mode == .category {
            let storyboard = UIStoryboard(name: "Employee", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
            vc.mode = .subCategory
            self.navigationController?.pushViewController(vc, animated: true)
        } else if mode == .subCategory {
            performSegue(withIdentifier: "showJobTypes", sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
