//
//  Validation.swift
//  RAF
//
//  Created by Hadi Sharghi on 4/2/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import Foundation
//import Validator

struct ValidationError: Error {
    let message: String
    init(messageString m: String) {
        message = m
    }

    init(message m: ValidationMessages) {
        message = m.message
    }
    
    
}


enum ValidationMessages: String {
    typealias RawValue = String


    case emptyError = "Field can not be empty"
    case emailInvalid = "Invalid email"
    case companyNameInvalid = "Invalid company name"
    case addressInvalid = "Invalid address"
    case countryCodeInvalid = "Invalid country code"
    case mobileNumberInvalid = "Invalid mobile number"
    case passwordLength = "Should be between 6 and 12 characters"
    case passwordCondition = "Should contains both alpha and numeric characters"
    case passwordNotMatch = "Passwords do not match"
    case acceptAgreement = "You must accept Terms & Conditions to proceed"
    var message: String { return self.rawValue }
}


public struct NumberOnlyValidationPattern: ValidationPattern {
    
    public init() {
        
    }
    
    public var pattern: String {
        return "^(0|[1-9][0-9]*)$"
    }
}


extension ValidationResult {
    func errorMessage() -> String? {
        switch self {
        case .valid:
            return nil
        case .invalid(let failures):
            let messages = failures.compactMap { $0 as? ValidationError }.map { $0.message }
            return messages.joined(separator: "")
        }
    }
}
