//
//  VerificationViewController.swift
//  RAF
//
//  Created by Hadi Sharghi on 6/23/18.
//  Copyright © 2018 Hadi Sharghi. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController, UITextFieldDelegate {
    
    let CodeDigits = 4
    @IBOutlet weak var codeEntryTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        codeEntryTextField.delegate = self
        codeEntryTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        [1,2,3,4].forEach { tag in
            if let textField = self.view.viewWithTag(tag) as? UITextField {
                textField.backgroundColor = UIColor.clear
                textField.layer.borderWidth = 1
                textField.layer.borderColor = UIColor.white.cgColor
                textField.layer.cornerRadius = 10
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        codeEntryTextField.becomeFirstResponder()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let code = textField.text {
            [1,2,3,4].forEach { tag in
                if let textField = self.view.viewWithTag(tag) as? UITextField {
                    textField.text = ""
                }
            }
            
            for i in 0..<code.count {
                let index = code.index(code.startIndex, offsetBy: i)
                if let codeTextField = self.view.viewWithTag(i+1) as? UITextField {
                    codeTextField.text = "\(code[index])"
                }
            }
        }
        
        if textField.text?.count ?? 0 == 4 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.verifyCode()
            })
        }
    }
    
    func verifyCode() {
        codeEntryTextField.resignFirstResponder()
        codeEntryTextField.isEnabled = false
        print("verifying...")
        sleep(2)
        codeEntryTextField.isEnabled = true
        codeEntryTextField.becomeFirstResponder()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

