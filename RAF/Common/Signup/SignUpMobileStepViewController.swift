//
//  SignUpMobileStepViewController.swift
//  RAF
//
//  Created by Hadi Sharghi on 4/2/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SignUpMobileStepViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var addressTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var countryCodeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AppData.addGradientLayer(to: self.view)
        nextButton.flip()
        
        addressTextField.errorColor = .red
        mobileTextField.errorColor = .red
    }
    
    @IBAction func viewTapped(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let t = textField as? SkyFloatingLabelTextField {
            t.errorMessage = nil
        }
        
        return true
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func verifyFields() -> Bool {
        var msg = addressTextField.validate(rule: AppData.addressValidationRule).errorMessage()
        guard msg == nil else {
            addressTextField.errorMessage = msg
            return false
        }
        
        msg = mobileTextField.validate(rule: AppData.mobileValidationRule).errorMessage()
        guard msg == nil else {
            mobileTextField.errorMessage = msg
            return false
        }
        
        return true
        
    }

    @IBAction func nextButtonTapped(_ sender: Any) {
        if verifyFields() {
            performSegue(withIdentifier: "nextStep", sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
