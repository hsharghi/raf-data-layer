//
//  SignUpPasswordStepViewController.swift
//  RAF
//
//  Created by Hadi Sharghi on 4/2/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Validator

class SignUpPasswordStepViewController: UIViewController, UITextFieldDelegate {

    var isAgree : Bool = false {
        didSet {
            let image = isAgree ? UIImage(named: "tick-inside-circle") : UIImage(named: "circle")
            agreeButton.setImage(image, for: .normal)
            agreementErrorLabel.isHidden = true
        }
    }
    
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordConfirmationTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var agreementErrorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppData.addGradientLayer(to: self.view)
        nextButton.setImage(nil, for: .normal)
        nextButton.setTitle("Register", for: .normal)
        
        agreementErrorLabel.text = ""
        agreementErrorLabel.isHidden = true
        
        passwordTextField.errorColor = .red
        passwordConfirmationTextField.errorColor = .red
        
    }
    
    @IBAction func viewTapped(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let t = textField as? SkyFloatingLabelTextField {
            t.errorMessage = nil
        }
        
        return true
    }
    
    
    func verifyFields() -> Bool {
        
        var msg = passwordTextField.validate(rule: AppData.passwordLengthValidationRule).errorMessage()
        guard msg == nil else {
            passwordTextField.errorMessage = msg
            return false
        }
        
        let passwordMatchRule =  ValidationRuleEquality<String>(dynamicTarget: { return self.passwordTextField.text ?? "" }, error: ValidationError(message: ValidationMessages.passwordNotMatch))
        msg = passwordConfirmationTextField.validate(rule: passwordMatchRule).errorMessage()
        guard msg == nil else {
            passwordConfirmationTextField.errorMessage = msg
            return false
        }
        
        
        guard isAgree else {
            agreementErrorLabel.text = ValidationMessages.acceptAgreement.message
            agreementErrorLabel.isHidden = false
            return false
        }

        return true
    }
    
    func register() {
        print("registerin...")
    }
    
    
    @IBAction func agreeButtonTapped(_ sender: Any) {
        isAgree = !isAgree
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if verifyFields() {
            register()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
