//
//  SignUpEmailStepViewController.swift
//  RAF
//
//  Created by Hadi Sharghi on 4/2/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Validator

class SignUpEmailStepViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var companyNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var nextButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppData.addGradientLayer(to: self.view)
        nextButton.flip()
        
        companyNameTextField.errorColor = .red
        emailTextField.errorColor = .red

        
    }

    @IBAction func viewTapped(_ sender: Any) {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let t = textField as? SkyFloatingLabelTextField {
            t.errorMessage = nil
        }
        
        return true
    }
    
    
    @IBAction func socialSignUpButtonTapped(_ sender: Any) {
    }
    
    func verifyFields() -> Bool {
        var msg = companyNameTextField.validate(rule: AppData.companyNameValidationRule).errorMessage()
        guard msg == nil else {
            companyNameTextField.errorMessage = msg
            return false
        }
        
        msg = emailTextField.validate(rule: AppData.emailValidationRule).errorMessage()
        guard msg == nil else {
            emailTextField.errorMessage = msg
            return false
        }
        
        return true
        
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if verifyFields() {
            performSegue(withIdentifier: "nextStep", sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
