//
//  VerificationFirstStepViewController.swift
//  RAF
//
//  Created by Hadi Sharghi on 4/2/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import UIKit

class VerificationFirstStepViewController: UIViewController {

    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppData.addGradientLayer(to: self.view)
        nextButton.alpha = 0
        nextButton.isUserInteractionEnabled = false
    }
    
    @IBAction func sendVerificationCodeViaEmail(_ sender: Any) {
        print("requesting verification code...")
        nextButtonTapped(sender)
    }
    
    @IBAction func sendVerificationCodeViaSMS(_ sender: Any) {
        print("requesting verification code...")
        nextButtonTapped(sender)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "nextStep", sender: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
