//
//  VerificationFinalStepViewController.swift
//  RAF
//
//  Created by Hadi Sharghi on 4/2/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import UIKit

class VerificationFinalStepViewController: UIViewController, UITextFieldDelegate {
    
    var emailAddress = "email address"
    let CodeDigits = 4
    
    @IBOutlet weak var codeEntryTextField: UITextField!
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var navigationStackBottomAnchor: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        codeEntryTextField.delegate = self
        codeEntryTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        infoLabel.text = infoLabel.text?.replacingOccurrences(of: "@@", with: emailAddress)
        
        [1,2,3,4].forEach { tag in
            if let textField = self.view.viewWithTag(tag) as? UITextField {
                textField.backgroundColor = UIColor.clear
                textField.layer.borderWidth = 1
                textField.layer.borderColor = UIColor.white.cgColor
                textField.layer.cornerRadius = 10
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.navigationStackBottomAnchor.constant += keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.navigationStackBottomAnchor.constant -= keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }

    @IBAction func viewTapped(_ sender: Any) {
        if codeEntryTextField.isFirstResponder {
            self.view.endEditing(true)
        } else {
            codeEntryTextField.becomeFirstResponder()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    override func viewWillAppear(_ animated: Bool) {
        codeEntryTextField.becomeFirstResponder()
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        if let code = textField.text {
            [1,2,3,4].forEach { tag in
                if let textField = self.view.viewWithTag(tag) as? UITextField {
                    textField.text = ""
                }
            }
            
            for i in 0..<code.count {
                let index = code.index(code.startIndex, offsetBy: i)
                if let codeTextField = self.view.viewWithTag(i+1) as? UITextField {
                    codeTextField.text = "\(code[index])"
                }
            }
        }
        
        if textField.text?.count ?? 0 == 4 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.verifyCode()
            })
        }
    }

    func verifyCode() {
        codeEntryTextField.resignFirstResponder()
        codeEntryTextField.isEnabled = false
        print("verifying...")
        sleep(2)
        codeEntryTextField.isEnabled = true
        codeEntryTextField.becomeFirstResponder()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

