//
//  Lists.swift
//  RAF
//
//  Created by Ali Torabi on 8/5/18.
//  Copyright © 2018 Hadi Sharghi. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper


enum ListType: Int {
    case industry = 1
    case candidate = 2
    case jobTitle_Type = 3
    case nationality = 4
    case residency = 5
    case degree = 6
    case employmentType = 7
}

enum CategoryType: Int {
    case skill = 1
    case hobby = 2
    case jobRole = 3
}

class PaginatedList: MappableData {
    var data : [ListItem]?
    var pages: Int?
    var rows: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
        pages <- map["totalPageCount"]
        rows <- map["totalRowCount"]
    }
    
}

class ListItem: MappableData {
    var id: Int?
    var name: String?
    private var dateCreatedString: String?
    var dateCreated: Date? {
        if let dateStr = dateCreatedString {
            let dateFromatter = DateFormatter()
            dateFromatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            return dateFromatter.date(from: dateStr)
        } else {
            return nil
        }
    }
    
    var type: Int?
    var status: Int?
    var parentLists : [String]?
    var typeName: String?
    var statusName: String?
    var language: Int?
    var iconUrl: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["key"]
        name <- map["name"]
        type <- map["type"]
        typeName <- map["typeName"]
        language <- map["language"]
        dateCreatedString <- map["lsDateCreated"]
        iconUrl <- map["urlIcon"]
        status <- map["lsStatus"]
        parentLists <- map["rafParentLists"]
        statusName <- map["lsStatus_Name"]

    }
    
    
    
    class func lists(type: ListType, page: Int = 1, perPage: Int = 10, _ dataHandler: @escaping (_ lists:[ListItem]?/*, _ error: AppError?*/) -> Void) -> () {
        
        //        http://raf.adad.ws/api/RafLists?PageNumber=1&PageCount=10&Type=1
        
        let url = AppData.apiUrl + "RafLists?PageNumber=\(page)&PageCount=\(perPage)&Type=\(type.rawValue)"
        
        let headers : HTTPHeaders = [:]
        
        Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<PaginatedList>) in
            switch response.result {
                
            case .success(let paginatedData):
                //                guard data.errors == nil else {
                //                    dataHandler(nil, data.errors)
                //                    return
                //                }
                //
                dataHandler(paginatedData.data)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [netError.localizedDescription])
                    dataHandler(nil)
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil) /*,AppError(errorCode: response.response!.statusCode, message: rawString))*/
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }
    }
    
    
    
    
    class func category(type: CategoryType, page: Int = 1, perPage: Int = 10, _ dataHandler: @escaping (_ lists:[ListItem]?/*, _ error: AppError?*/) -> Void) -> () {
        
        //        http://raf.adad.ws/api/RafLists?PageNumber=1&PageCount=10&Type=1
        
        let url = AppData.apiUrl + "RafCategories?PageNumber=\(page)&PageCount=\(perPage)&Type=\(type.rawValue)"
        
        let headers : HTTPHeaders = [:]
        
        Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<PaginatedList>) in
            switch response.result {
                
            case .success(let paginatedData):
                //                guard data.errors == nil else {
                //                    dataHandler(nil, data.errors)
                //                    return
                //                }
                //
                dataHandler(paginatedData.data)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [netError.localizedDescription])
                    dataHandler(nil)
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil) /*,AppError(errorCode: response.response!.statusCode, message: rawString))*/
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }
    }
}


