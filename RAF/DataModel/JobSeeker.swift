//
//  JobSeeker.swift
//  RAF
//
//  Created by Hadi Sharghi on 5/13/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper


class ExperienceList: MappableData {
    var data: [Experience]?
    override func mapping(map: Map) {
        data <- map["data"]
    }
}

class Degree: Mappable {
    var id : Int?
    var name: String?
    var icon: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["key"]
        name <- map["name"]
        icon <- map["urlIcon"]
    }
}


class JobRole: Mappable {
    var id : Int?
    var name: String?
    var icon: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["key"]
        name <- map["name"]
        icon <- map["urlIcon"]
    }
}

class Experience: MappableData {
    
    enum ExperienceType: Int {
        case education = 1
        case work = 2
    }
    
    var id: Int?
    var type: ExperienceType?
    private var experienceType: Int?
    var name: String?
    var location: String?
    var degree: Degree?
    var jobRole: JobRole?
    var title: String?
    var gba: String?
    private var fromTimestamp: Int?
    private var toTimestamp: Int?
    var from: Date?
    var to: Date?
    var duration: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        location <- map["location"]
        name <- map["name"]
        degree <- map["degree"]
        jobRole <- map["jobRole"]
        title <- map["title"]
        gba <- map["gba"]
        from <- map["expKey"]
        experienceType <- map["type"]
        if let _ = experienceType {
            type = experienceType! == 1 ? ExperienceType.education : ExperienceType.work
        }
    }
}


class JobSeekerProfile: MappableData {

    enum gender: Int {
        case male = 1
        case female = 0
    }

    var id: Int?
    var civilId: String?
    var nationality: Int?
    var residency: Int?
    private var birthdayTimestamp: Int64?
    var birthday: Date?
    private var genderType: Bool?
    var gender: gender?
    var bio : String?
    var website: String?
    var language: Int?
    var fullName: String?
    var referralCode: String?
    var workExperiences: [Experience]?
    var educationExperiences: [Experience]?
    var industries: [ListItem]?
    var skills: [ListItem]?
    var hobbies: [ListItem]?
    var jobRoles: [ListItem]?
    var documents: [Document]?

    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["key"]
        civilId <- map["civilId"]
        nationality <- map["nationality"]
        residency <- map["residency"]
        birthdayTimestamp <- map["birthday"]
        if let timestamp = birthdayTimestamp {
            birthday = Date(timeIntervalSince1970: TimeInterval(timestamp))
        }
        genderType <- map["gender"]
        if let gender = genderType {
            self.gender = gender ? .male : .female
        }
        
        bio <- map["bio"]
        website <- map["website"]
        language <- map["language"]
        fullName <- map["fullName"]
        referralCode <- map["referralCode"]
        workExperiences <- map["experienceWork"]
        educationExperiences <- map["experienceEduction"]
        industries <- map["industry"]
        skills <- map["skills"]
        hobbies <- map["hobbies"]
        jobRoles <- map["roles"]
        documents <- map["document"]

    }
    
    class func get(_ dataHandler: @escaping (_ profile: JobSeekerProfile? /*, _ error: AppError?*/) -> Void) -> () {
        
        let url = AppData.apiUrl + "RafProfiles"
        
        guard AppData.token != nil else {
            return
        }
        
        let headers : HTTPHeaders = [
            "Authorization" : "Bearer \(AppData.token!)"
        ]
        
        Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<JobSeekerProfile>) in
            switch response.result {
                
            case .success(let profile):
                //                guard data.errors == nil else {
                //                    dataHandler(nil, data.errors)
                //                    return
                //                }
                //
                dataHandler(profile)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [netError.localizedDescription])
                    dataHandler(nil)
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil) /*,AppError(errorCode: response.response!.statusCode, message: rawString))*/
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }
    }
    
    
    class func update(fullName: String? = nil, civilId:String? = nil, nationality: Int? = nil, residency: Int? = nil,
                      birthday: Date? = nil, gender: Int? = nil, bio: String? = nil, website: String? = nil, languageId: Int? = nil,
                      _ dataHandler: @escaping (_ profile: JobSeekerProfile? , _ error: ResponseError?) -> Void) -> () {
        
        let url = AppData.apiUrl + "RafJseekersProfiles"

        guard AppData.token != nil else {
            return
        }
        
        guard AppData.userRole == User.Role.jobSeeker else {
            return
        }
        
        
        let headers : HTTPHeaders = [
            "Authorization" : "Bearer \(AppData.token!)"
        ]
        
        let parameters:[String:String] = [
            "FullName" : fullName ?? "",
            "CivilId" : civilId ?? "",
            "Nationality" : nationality != nil ? "\(nationality!)" : "",
            "Residency" : residency != nil ? "\(residency!)" : "",
            "Birthday" : birthday?.toEpochMilliSecString() ?? "",
            "Gender" : gender != nil ? "\(gender!)" : "",
            "Bio" : bio ?? "",
            "Website" : website ?? "",
            "Language" : languageId != nil ? "\(languageId!)" : "",
            ]
        
        
        Alamofire.request(url, method: .put, parameters: parameters, headers: headers).responseObject { (response: DataResponse<JobSeekerProfile>) in
            switch response.result {
                
            case .success(let profile):
            guard profile.errors == nil else {
                dataHandler(nil, profile.errors?.first)
                return
            }

                dataHandler(profile, nil)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [netError.localizedDescription])
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                          message: "\(response.response!.statusCode)")]
                            ))
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }
    }
    
    
    
    class func addIndustry(industries: [Int]? = nil, _ dataHandler: @escaping (_ profile: [ListItem]? /*, _ error: AppError?*/) -> Void) -> () {
        
        let url = AppData.apiUrl + "RafJseekersIndustries" + "?Type=\(ListType.industry.rawValue)"

        guard AppData.token != nil else {
            return
        }
        
        guard AppData.userRole == User.Role.jobSeeker else {
            return
        }
        
        
        let headers : HTTPHeaders = [
            "Authorization" : "Bearer \(AppData.token!)"
        ]
        
        let parameters: Parameters = [
            "rafJseekersIndustry" : industries ?? [],
            ]
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<PaginatedList>) in
            switch response.result {
                
            case .success(let list):
                //                guard data.errors == nil else {
                //                    dataHandler(nil, data.errors)
                //                    return
                //                }
                //
                dataHandler(list.data)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [netError.localizedDescription])
                    dataHandler(nil)
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil) /*,AppError(errorCode: response.response!.statusCode, message: rawString))*/
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }
    }
    
    class func addCategory(type: CategoryType, ids: [Int]? = nil,   _ dataHandler: @escaping (_ profile: [ListItem]? /*, _ error: AppError?*/) -> Void) -> () {
        
        let url = AppData.apiUrl + "RafJseekersCategories" + "?Type=\(type.rawValue)"
        
        guard AppData.token != nil else {
            return
        }
        
        guard AppData.userRole == User.Role.jobSeeker else {
            return
        }
        
        
        let headers : HTTPHeaders = [
            "Authorization" : "Bearer \(AppData.token!)"
        ]
        
        let parameters: Parameters = [
            "rafJseekersCategories" : ids ?? [],
            ]
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<PaginatedList>) in
            switch response.result {
                
            case .success(let list):
                //                guard data.errors == nil else {
                //                    dataHandler(nil, data.errors)
                //                    return
                //                }
                //
                dataHandler(list.data)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [netError.localizedDescription])
                    dataHandler(nil)
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil) /*,AppError(errorCode: response.response!.statusCode, message: rawString))*/
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }
    }
    
    
    class func getExperiences(type: Experience.ExperienceType, _ dataHandler: @escaping (_ experiences: [Experience]? /*, _ error: AppError?*/) -> Void) -> () {
        
        
        let url = AppData.apiUrl + "RafProfiles"
        
        guard AppData.token != nil else {
            return
        }
        
        let headers : HTTPHeaders = [
            "Authorization" : "Bearer \(AppData.token!)"
        ]
        
        let parameters = [
            "type" : type.rawValue
        ]
        
        Alamofire.request(url, parameters: parameters, headers: headers).responseObject { (response: DataResponse<ExperienceList>) in
            switch response.result {
                
            case .success(let experienceList):
                //                guard data.errors == nil else {
                //                    dataHandler(nil, data.errors)
                //                    return
                //                }
                //
                dataHandler(experienceList.data)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [netError.localizedDescription])
                    dataHandler(nil)
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil) /*,AppError(errorCode: response.response!.statusCode, message: rawString))*/
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }
    }
    

    class func addExperience(type: Experience.ExperienceType, name: String? = nil, location:String? = nil, degreeType: Int? = nil, degreeTitle: String? = nil, gba: String? = nil, jobRole: Int? = nil, from: Date? = nil, to: Date? = nil, _ dataHandler: @escaping (_ profile: Experience? /*, _ error: AppError?*/) -> Void) -> () {
        
        let url = AppData.apiUrl + "RafJseekersExperiences"
        
        guard AppData.token != nil else {
            return
        }
        
        guard AppData.userRole == User.Role.jobSeeker else {
            return
        }
        
        
        let headers : HTTPHeaders = [
            "Authorization" : "Bearer \(AppData.token!)"
        ]
        
        let parameters:[String:String] = [
            "Name" : name ?? "",
            "Location" : location ?? "",
            "DegreeType" : degreeType != nil ? "\(degreeType!)" : "",
            "JobRole" : jobRole != nil ? "\(jobRole!)" : "",
            "Title" : degreeTitle ?? "",
            "From" : from?.toEpochMilliSecString() ?? "",
            "To" : to?.toEpochMilliSecString() ?? "",
            "Gba" : gba ?? "",
            "Type" : "\(type.rawValue)",
            ]
        
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Experience>) in
            switch response.result {
                
            case .success(let experience):
                //                guard data.errors == nil else {
                //                    dataHandler(nil, data.errors)
                //                    return
                //                }
                //
                dataHandler(experience)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [netError.localizedDescription])
                    dataHandler(nil)
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil) /*,AppError(errorCode: response.response!.statusCode, message: rawString))*/
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }
    }
    
    

}
