//
//  Document.swift
//  RAF
//
//  Created by Hadi Sharghi on 5/13/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

class Document: MappableData {
    var id: Int?
    var title: String?
    var type: String?
    var path: String?
    var url: String?
    var mimeType: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["docKey"]
        title <- map["docName"]
        path <- map["docPath"]
        url <- map["docUrl"]
        type <- map["docType"]
        mimeType <- map["docExtension"]
    }
    
    
    class func list(token: String, _ dataHandler: @escaping (_ response: [Document]?, _ errors: ResponseError?) -> Void) {
        
        let url = AppData.apiUrl + "RafDocuments/GetFileList"
        
        var parameters: Parameters = [:]
        if let role = AppData.userRole {
            let tableKey = role == .jobSeeker ? 1 : 2
            parameters = [
                "TableKey": "\(tableKey)",
            ]
        }
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer " + token,
            ]
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers)
                /*
            //            .responseObject { (response: DataResponse<[Document]>) in
            //            switch response.result {
            //
            //            case .success(let data):
            //                guard data.errors == nil else {
            //                    dataHandler(nil, data.errors)
            //                    return
            //                }
            //
            //            case .failure(let error):
            //                if let netError = error as? URLError {
            //                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
            //                    //                    dataHandler(nil, [ResponseError(code: "\(netError.code.rawValue)", description: netError.localizedDescription)])
            //                    dataHandler(nil, [netError.localizedDescription])
            //                    return
            //                } else {
            //                    print("Can't map to anything, can't find error type, now just print out the raw response")
            //                }
            //            }
            //            // if no dataHandler has been created so far, map the response to string...
            //            }
            */
            .responseArray { (response: DataResponse<[Document]>) in
                switch response.result {
                    
                case .success(let data):
                    dataHandler(data, nil)
                    
                case .failure(let error) :
                    
                    if let netError = error as? URLError {
                        print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                        //                    dataHandler(nil, [ResponseError(code: "\(netError.code.rawValue)", description: netError.localizedDescription)])
//                        dataHandler(nil, [netError.localizedDescription])
                        return
                    } else {
                        print("Can't map to anything, can't find error type, now just print out the raw response")
                    }
                }
            }
            .responseString { (response: DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500:
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        // normally wouldn't happen!, but...
                        print(#function, "WTF?!")
                    default:
                        print("Got the String response...")
                        print(rawString.count, rawString)
                        print("Status code: ")
                        print(response.response!.statusCode)
                    }
                }
        }
        
    }
    
    enum DocType: Int {
        case profilePicture = 1
        case image = 2
        case pdf = 3
    }
    
    class func upload(doc: Data, docType: DocType, docName: String?, _ dataHandler: @escaping (_ result:ServerResponse?, _ error: [String]?) -> Void) -> () {
        
        guard AppData.token != nil else {
            return
        }
        
        guard AppData.userRole != nil && (AppData.userRole == .jobSeeker || AppData.userRole == .employer) else {
            return
        }

        var mimeType = ""
        switch docType {
        case .image, .profilePicture:
            mimeType = "image/png"
        case .pdf:
            mimeType = "application/pdf"
        }
        
        let headers : HTTPHeaders = [
            "Content-Type": "multipart/form-data",
            "Authorization" : "Bearer " + AppData.token!
        ]

        let parameters: Parameters = [
            "DocType" : docType.rawValue,
            "DocName" : docName ?? ""
        ]

        let url = AppData.apiUrl + "RafDocuments/UploadFile"

        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            multipartFormData.append(doc, withName: "FileToUpload", fileName: docName ?? "", mimeType: mimeType)
                //Debug message:
//                print(docUrl.lastPathComponent)
        },
                         to:url, method: .post, headers: headers)
        { (result) in
            //print(result)
            switch result {
            case .success(let upload, _, _):
                print("Upload Success")
                upload.responseObject {(response: DataResponse<ServerResponse>) in
                    print(response.result.description)
                    if let profile = response.result.value {
                        print("Inside ObjectMapper -\(response.response!.statusCode)")
                        switch response.response!.statusCode {
                        case 200..<400 :
                            dataHandler(profile, nil)
                        default:
                            print("Server returned:\(response.response!.statusCode)")
                        }
                    }
                    }.responseJSON { (response) in
                        print(response.response.debugDescription)
                        if let json = response.result.value {
                            print(json)
                            switch response.response!.statusCode {
                            case 200..<400 :
                                //Mark: Debug message
                                print(json)
                                return
                            default:
                                dataHandler(nil,json as? [String])
                            }
                        }
                    }.responseString(completionHandler: { (response) in
                        print(response.description)
                    })
            case .failure(let encodingError):
                print("Futile Effort!")
                print(encodingError)
                dataHandler(nil, encodingError as? [String])
            }
            
        }
    }


}



