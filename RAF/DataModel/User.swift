//
// Created by Hadi Sharghi on 7/11/2018.
// Copyright (c) 2018 Hadi Sharghi. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper


class Profile: MappableData {
    
    var id: Int?
    var fullName: String?
    var email: String?
    var phoneNumber: String?
    var status: User.Status?
    var role: User.Role?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        email <- map["Email"]
        fullName <- map["FullName"]
        phoneNumber <- map["PhoneNumber"]
        status <- map["Status"]
        role <- map["Role_id"]
    }
    
}

class Token: MappableData {
    var tokenString: String?
    private var expireDateString: String?
    var expireDate: Date?
    var code: String?
    var phoneNumberConfirmed: Bool?
    var emailConfirmed: Bool?
    var profileCompleted: Bool?
    var profileCompletionPercentage: Int?
    override func mapping(map: Map) {
        super.mapping(map: map)
        tokenString <- map["token"]
        expireDateString <- map["expires"]
        if let _ = expireDateString {
            expireDate = Date(timeIntervalSince1970: Double(expireDateString!)!)
        }
        code <- map["code"]
        phoneNumberConfirmed <- map["phoneNumberConfirmed"]
        emailConfirmed <- map["emailConfirmed"]
        profileCompleted <- map["profileCompleted"]
        profileCompletionPercentage <- map["percentageProfile"]
    }
}

class User: MappableData {
    
    
    enum Status: Int {
        case active = 1
        case inActive = 2
        case suspend = 3
    }
    
    enum Role: Int {
        case superAdmin = 1
        case managementAdmin = 2
        case employer = 3
        case jobSeeker = 4
    }
    
    struct LoginStatus: Codable {
        var tokenExpire: Date?
        var phoneNumberConfirmed: Bool?
        var emailConfirmed: Bool?
        var profileCompletionPercentage: Int?
        var profileCompleted: Bool?
    }

    struct RegisterData:Codable {
        var email: String?
        var fullName: String?
        var address: String?
        var countryCode: String?
        var phoneNumber: String?
        var password: String?
        var temporaryToken: String?
        var verificationCode: String?
        
    }
    
    class func register(phoneNumber: String, fullName: String, email: String? = nil, _ dataHandler: @escaping (_ response: Token?, _ errors: ResponseError?) -> Void) {
        
        guard AppData.userRole != nil && (AppData.userRole == .jobSeeker || AppData.userRole == .employer) else {
            return
        }
        
        
        let url = AppData.apiUrl + "Auth/Register"
        
        let parameters: Parameters = [
            "Email": email ?? "",
            "FullName": fullName,
            "PhoneNumber": phoneNumber,
            "Status" : "\(User.Status.active.rawValue)",
            "RoleId" : "\(AppData.userRole!.rawValue)"
        ]
    
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            //            "Authorization": "Bearer " + (AppData.token ?? ""),
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Token>) in
            switch response.result {
                
            case .success(let data):
                guard data.errors == nil else {
                    dataHandler(nil, data.errors?.first)
                    return
                }
                
                dataHandler(data, nil)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [ResponseError(code: "\(netError.code.rawValue)", description: netError.localizedDescription)])
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            // if no dataHandler has been created so far, map the response to string...
        }.responseString { (response: DataResponse<String>) in
            if let rawString = response.result.value {
                switch response.response!.statusCode {
                case 300...500:
                    print(response.response!.statusCode)
                    dataHandler(nil, ResponseError(message: rawString,
                                                   fields: [FieldError(field: "statusCode",
                                                                       message: "\(response.response!.statusCode)")]
                    ))
                case 200:
                    // normally wouldn't happen!, but...
                    print(#function, "WTF?!")
                default:
                    print("Got the String response...")
                    print(rawString.count, rawString)
                    print("Status code: ")
                    print(response.response!.statusCode)
                }
            }
        }

    }

    class func login(phoneNumber: String, _ dataHandler: @escaping (_ token: Token?, _ errors: ResponseError?) -> Void) {
        
        guard AppData.userRole != nil && (AppData.userRole == .jobSeeker || AppData.userRole == .employer) else {
            return
        }
        
        let url = AppData.apiUrl + "Auth/Login"
        
        let parameters: Parameters = [
            "PhoneNumber": phoneNumber,
            "RoleId" : "\(AppData.userRole!.rawValue)"
        ]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            ]
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Token>) in
            switch response.result {
                
            case .success(let data):
//                guard data.errors == nil else {
//                    dataHandler(nil, data.errors)
//                    return
//                }
                
                dataHandler(data, nil)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            // if no dataHandler has been created so far, map the response to string...
            }.responseString { (response: DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500:
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        // normally wouldn't happen!, but...
                        print(#function, "WTF?!")
                    default:
                        print("Got the String response...")
                        print(rawString.count, rawString)
                        print("Status code: ")
                        print(response.response!.statusCode)
                    }
                }
        }
        
    }
    
    
    class func changePassword(oldPassword: String, newPassword: String, _ dataHandler: @escaping (_ response: ServerResponse?, _ errors: ResponseError?) -> Void) {
        
        let url = AppData.apiUrl + "User/ChangePassword"
        
        let parameters: Parameters = [
            "OldPassword": oldPassword,
            "NewPassword": newPassword,
            ]
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + (AppData.token ?? ""),
            "Content-Type": "application/x-www-form-urlencoded",
            ]
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<ServerResponse>) in
            switch response.result {
                
            case .success(let data):
                guard data.errors == nil else {
                    dataHandler(nil, data.errors?.first)
                    return
                }
                
                dataHandler(data, nil)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [ResponseError(code: "\(netError.code.rawValue)", description: netError.localizedDescription)])
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            // if no dataHandler has been created so far, map the response to string...
            }.responseString { (response: DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500:
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        // normally wouldn't happen!, but...
                        print(#function, "WTF?!")
                    default:
                        print("Got the String response...")
                        print(rawString.count, rawString)
                        print("Status code: ")
                        print(response.response!.statusCode)
                    }
                }
        }
        
    }
    
    
    class func resetPassword(email: String, _ dataHandler: @escaping (_ response: ServerResponse?, _ errors: ResponseError?) -> Void) {
        
        let url = AppData.apiUrl + "Auth/GenerateResetPasswordLink"
        
        let parameters: Parameters = [
            "Email": email,
            ]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            ]
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<ServerResponse>) in
            switch response.result {
                
            case .success(let data):
                guard data.errors == nil else {
                    dataHandler(nil, data.errors?.first)
                    return
                }
                
                dataHandler(data, nil)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [ResponseError(code: "\(netError.code.rawValue)", description: netError.localizedDescription)])
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            // if no dataHandler has been created so far, map the response to string...
            }.responseString { (response: DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500:
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        // normally wouldn't happen!, but...
                        print(#function, "WTF?!")
                    default:
                        print("Got the String response...")
                        print(rawString.count, rawString)
                        print("Status code: ")
                        print(response.response!.statusCode)
                    }
                }
        }
        
    }
    
    
    
    class func sendEmailValidationCode(_ dataHandler: @escaping (_ response: ServerResponse?, _ errors: ResponseError?) -> Void) {
        
        let url = AppData.apiUrl + "User/GenerateConfirmEmailLink"
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + (AppData.token ?? ""),
            "Content-Type": "application/x-www-form-urlencoded",
            ]
        
        Alamofire.request(url, method: .post, headers: headers).responseObject { (response: DataResponse<ServerResponse>) in
            switch response.result {
                
            case .success(let data):
                guard data.errors == nil else {
                    dataHandler(nil, data.errors?.first)
                    return
                }
                
                dataHandler(data, nil)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [ResponseError(code: "\(netError.code.rawValue)", description: netError.localizedDescription)])
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            // if no dataHandler has been created so far, map the response to string...
            }.responseString { (response: DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500:
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        // normally wouldn't happen!, but...
                        print(#function, "WTF?!")
                    default:
                        print("Got the String response...")
                        print(rawString.count, rawString)
                        print("Status code: ")
                        print(response.response!.statusCode)
                    }
                }
        }
        
    }
    
    class func validateEmail(confirmationCode: String, _ dataHandler: @escaping (_ response: ServerResponse?, _ errors: ResponseError?) -> Void) {
        
        let url = AppData.apiUrl + "ApiAccountController/EmailConfirmed"
        
        let parameters: Parameters = [
            "CnCode": confirmationCode,
            ]
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + (AppData.token ?? ""),
            "Content-Type": "application/x-www-form-urlencoded",
            ]
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<ServerResponse>) in
            switch response.result {
                
            case .success(let data):
                guard data.errors == nil else {
                    dataHandler(nil, data.errors?.first)
                    return
                }
                
                dataHandler(data, nil)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            // if no dataHandler has been created so far, map the response to string...
            }.responseString { (response: DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500:
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                        print(response.response!.statusCode)
                    case 200:
                        // normally wouldn't happen!, but...
                        print(#function, "WTF?!")
                    default:
                        print("Got the String response...")
                        print(rawString.count, rawString)
                        print("Status code: ")
                        print(response.response!.statusCode)
                    }
                }
        }
        
    }




    class func addPhoneNumber(phoneNumber: String, token: String, _ dataHandler: @escaping (_ response: ServerResponse?, _ errors: ResponseError?) -> Void) {

        let url = AppData.apiUrl + "User/AddPhoneNumber"

        let parameters: Parameters = [
            "PhoneNumber": phoneNumber,
        ]

        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/x-www-form-urlencoded",
        ]

        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<ServerResponse>) in
            switch response.result {

            case .success(let data):
                guard data.errors == nil else {
                    dataHandler(nil, data.errors?.first)
                    return
                }

                dataHandler(data, nil)

            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            // if no dataHandler has been created so far, map the response to string...
        }.responseString { (response: DataResponse<String>) in
            if let rawString = response.result.value {
                switch response.response!.statusCode {
                case 300...500:
                    print(response.response!.statusCode)
                    dataHandler(nil, ResponseError(message: rawString,
                                                   fields: [FieldError(field: "statusCode",
                                                                       message: "\(response.response!.statusCode)")]
                    ))
                case 200:
                    // normally wouldn't happen!, but...
                    print(#function, "WTF?!")
                default:
                    print("Got the String response...")
                    print(rawString.count, rawString)
                    print("Status code: ")
                    print(response.response!.statusCode)
                }
            }
        }

    }



    class func validatePhoneNumber(confirmationCode: String, temporaryToken: String, _ dataHandler: @escaping (_ response: Token?, _ errors: ResponseError?) -> Void) {
        
        let url = AppData.apiUrl + "User/VerifyPhoneNumber"
        
        let parameters: Parameters = [
            "Code": confirmationCode,
            ]
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + temporaryToken,
            "Content-Type": "application/x-www-form-urlencoded",
            ]
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Token>) in
            switch response.result {
                
            case .success(let data):
                guard data.errors == nil else {
                    dataHandler(nil, data.errors?.first)
                    return
                }
                
                dataHandler(data, nil)
                
            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    dataHandler(nil, ResponseError(message: "Error", fields: nil))
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            // if no dataHandler has been created so far, map the response to string...
            }.responseString { (response: DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500:
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        // normally wouldn't happen!, but...
                        print(#function, "WTF?!")
                    default:
                        print("Got the String response...")
                        print(rawString.count, rawString)
                        print("Status code: ")
                        print(response.response!.statusCode)
                    }
                }
        }
        
    }
    
    
}

