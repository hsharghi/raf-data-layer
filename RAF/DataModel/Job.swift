//
//  Lists.swift
//  RAF
//
//  Created by Ali Torabi on 8/5/18.
//  Copyright © 2018 Hadi Sharghi. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper


typealias Jobs = [Job]

class JobList: MappableData {
    var data : [Job]?
    var pages: Int?
    var rows: Int?

    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
        pages <- map["totalPageCount"]
        rows <- map["totalRowCount"]
    }
}

class Candidate: MappableData {
    var id: Int?
    var jobId: Int?
    var title: String?
    var degree: ListItem?
    var yearsOfExperience: Int?
    var gender: Int?
    private var genderBooleanValue: Bool? {
        didSet {
            if genderBooleanValue != nil {
                gender = genderBooleanValue! ? 1 : 0
            }
        }
    }

    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["key"]
        jobId <- map["jobKey"]
        title <- map["title"]
        degree <- map["degree"]
        yearsOfExperience <- map["yearDuration"]
        genderBooleanValue <- map["gender"]
    }

}

class Shift: MappableData {


    func createDateFromString(string: String) -> Date? {
        let components = string.components(separatedBy: ":")
        guard components.count == 3 else {
            return nil
        }
        let hour = Int(components[0]) ?? 0
        let min = Int(components[1]) ?? 0

        return Calendar.current.date(bySettingHour: hour, minute: min, second: 0, of: Date())
    }


    var id: Int?
    var jobId: Int?
    var weekDays: [Int]?
    private var daysString: String? {
        didSet {
            if let daysString = daysString {
                weekDays = daysString.toNumbers()
            }
        }
    }
    private var startShift1String: String?  {
        didSet {
            if let startShift1String = startShift1String {
                startShift1 = createDateFromString(string: startShift1String)
            }
        }
    }
    private var startShift2String: String?  {
        didSet {
            if let startShift2String = startShift2String {
                startShift2 = createDateFromString(string: startShift2String)
            }
        }
    }

    private var endShift1String: String?  {
        didSet {
            if let endShift1String = endShift1String {
                endShift1 = createDateFromString(string: endShift1String)
            }
        }
    }

    private var endShift2String: String?  {
        didSet {
            if let endShift2String = endShift2String {
                endShift2 = createDateFromString(string: endShift2String)
            }
        }
    }

    var startShift1: Date?
    var startShift2: Date?
    var endShift1: Date?
    var endShift2: Date?

    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["key"]
        jobId <- map["jobKey"]
        daysString <- map["days"]
        startShift1String <- map["startTime1"]
        startShift2String <- map["startTime2"]
        endShift1String <- map["endTime1"]
        endShift2String <- map["endTime2"]
    }

}


class Job: MappableData {


    var id: Int?
    var title: String?
    var role: JobRole?
    var industry: ListItem?
    var type: [ListItem]?
    var description: String?
    var numCandidates: Int?
    var maxApplicants: Int?
    var shifts: Shift?
    var candidates: Candidate?

    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["key"]
        title <- map["title"]
        role <- map["jobRole"]
        industry <- map["industry"]
        type <- map["type"]
        numCandidates <- map["candidatesNum"]
        maxApplicants <- map["maxApplication"]
        shifts <- map["shifts"]
        candidates <- map["candidate"]
    }


    class func get(page: Int = 1, perPage: Int = 10, _ dataHandler: @escaping (_ jobs:[Job]?, _ totalJobs: Int?,  _ error: ResponseError? ) -> Void) -> () {

        let url = AppData.apiUrl + "RafJob?PageNumber=\(page)&PageCount=\(perPage)"
        guard AppData.userRole == .employer, AppData.token != nil else {
            return
        }

        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer " + AppData.token!,
        ]

        Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<JobList>) in
            switch response.result {

            case .success(let paginatedData):
                //                guard data.errors == nil else {
                //                    dataHandler(nil, data.errors)
                //                    return
                //                }
                //
                dataHandler(paginatedData.data, paginatedData.rows, nil)

            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    //                    dataHandler(nil, [netError.localizedDescription])
                    dataHandler(nil, nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
        }.responseString { (response:DataResponse<String>) in
            if let rawString = response.result.value {
                switch response.response!.statusCode {
                case 300...500 :
                    print(response.response!.statusCode)
                    dataHandler(nil, nil, ResponseError(message: rawString,
                            fields: [FieldError(field: "statusCode",
                                    message: "\(response.response!.statusCode)")]
                    ))
                case 200:
                    if rawString == "" {
                        print("Empty String: No news is good news!")
                    }
                default:
                    print("Got the String response, but to little use.")
                    print(rawString.count, rawString)
                    print("Am I lost??")
                    print(response.response!.statusCode)
                }
            }
        }
    }

    class func create(title: String?, jobRoleId: Int, industryId: Int, jobTypes: [Int], description: String? = nil, numCandidates: Int, maxApplicants: Int, salary: Int, _ dataHandler: @escaping (_ jobs:Job?, _ error: ResponseError? ) -> Void) -> () {

        let url = AppData.apiUrl + "RafJob"
        guard AppData.userRole == .employer, AppData.token != nil else {
            return
        }

        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer " + AppData.token!,
        ]

        let parameters: Parameters = [
            "Title" : title ?? "",
            "Industry" : industryId,
            "JobRole" : jobRoleId,
            "Desc" : description ?? "",
            "CandidatesNum" : numCandidates,
            "MaxApplication" : maxApplicants,
            "Salary" : salary,
            "Type" : jobTypes
        ]

        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Job>) in
            switch response.result {

            case .success(let job):
                guard job.errors == nil else {
                    dataHandler(nil, job.errors?.first)
                    return
                }

                dataHandler(job, nil)

            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
        }.responseString { (response:DataResponse<String>) in
            if let rawString = response.result.value {
                switch response.response!.statusCode {
                case 300...500 :
                    print(response.response!.statusCode)
                    dataHandler(nil, ResponseError(message: rawString,
                            fields: [FieldError(field: "statusCode",
                                    message: "\(response.response!.statusCode)")]
                    ))
                case 200:
                    if rawString == "" {
                        print("Empty String: No news is good news!")
                    }
                default:
                    print("Got the String response, but to little use.")
                    print(rawString.count, rawString)
                    print("Am I lost??")
                    print(response.response!.statusCode)
                }
            }
        }
    }
    //TODO: Must talk, Do not try it yet!
    func updateJop(id: Int, title: String?, jobRoleId: Int, industryId: Int, jobTypes: [Int], description: String? = nil, numCandidates: Int, maxApplicants: Int, salary: Int, _ dataHandler: @escaping (_ job:Job?, _ error: ResponseError? ) -> Void) -> () {

        let url = AppData.apiUrl + "RafJob"
        guard AppData.userRole == .employer, AppData.token != nil else {
            return
        }

        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer " + AppData.token!,
            ]

        let parameters: Parameters = [
            "Title" : title ?? "",
            "Industry" : industryId,
            "JobRole" : jobRoleId,
            "Desc" : description ?? "",
            "CandidatesNum" : numCandidates,
            "MaxApplication" : maxApplicants,
            "Salary" : salary,
            "Type" : jobTypes
        ]

        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Job>) in
            switch response.result {

            case .success(let job):
                guard job.errors == nil else {
                    dataHandler(nil, job.errors?.first)
                    return
                }

                dataHandler(job, nil)

            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }
    }
    func addCandidateRequirements(title: String? = nil, degreeId: Int? = nil, yearsOfExperience: Int? = nil, gender: Int? = nil, _ dataHandler: @escaping (_ requirement: Candidate?, _ error: ResponseError? ) -> Void) -> () {

        let url = AppData.apiUrl + "RafJbTitleCandidates"
        guard AppData.userRole == .employer, AppData.token != nil, self.id != nil else {
            return
        }

        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer " + AppData.token!,
        ]

        var parameters: Parameters = [
            "JobKey": self.id!,
            "Title" : title ?? "",
            "Degree" : degreeId != nil ? "\(degreeId!)" : "",
            "YearDuration" : yearsOfExperience != nil ? "\(yearsOfExperience!)" : "",
        ]

        if gender != nil {
            parameters["Gender"] = gender! == 1 ? "true" : "false"
        }

        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Candidate>) in
            switch response.result {

            case .success(let candidate):
                guard candidate.errors == nil else {
                    dataHandler(nil, candidate.errors?.first)
                    return
                }

                dataHandler(candidate, nil)

            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
        }.responseString { (response:DataResponse<String>) in
            if let rawString = response.result.value {
                switch response.response!.statusCode {
                case 300...500 :
                    print(response.response!.statusCode)
                    dataHandler(nil, ResponseError(message: rawString,
                            fields: [FieldError(field: "statusCode",
                                    message: "\(response.response!.statusCode)")]
                    ))
                case 200:
                    if rawString == "" {
                        print("Empty String: No news is good news!")
                    }
                default:
                    print("Got the String response, but to little use.")
                    print(rawString.count, rawString)
                    print("Am I lost??")
                    print(response.response!.statusCode)
                }
            }
        }

    }
    //TODO: test
    class func updateCandidateRequirements(candidateID: Int, title: String? = nil, degreeId: Int? = nil, yearsOfExperience: Int? = nil, gender: Int? = nil, _ dataHandler: @escaping (_ requirement: Candidate?, _ error: ResponseError? ) -> Void) -> () {
        let url = AppData.apiUrl + "RafJbTitleCandidates?id=\(candidateID)"
        guard AppData.userRole == .employer, AppData.token != nil
            else {
            return
        }

        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer " + AppData.token!,
            ]

        var parameters: Parameters = [
            //"JobKey": self.id!,
            "Title" : title ?? "",
            "Degree" : degreeId != nil ? "\(degreeId!)" : "",
            "YearDuration" : yearsOfExperience != nil ? "\(yearsOfExperience!)" : "",
            ]
        if gender != nil {
            parameters["Gender"] = gender! == 1 ? "true" : "false"
        }
        Alamofire.request(url, method: .put, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Candidate>) in
            switch response.result {

            case .success(let candidate):
                guard candidate.errors == nil else {
                    dataHandler(nil, candidate.errors?.first)
                    return
                }

                dataHandler(candidate, nil)

            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    //Why not map/parse 'errors' here?
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }

    }

    class func dateToTimeString(date: Date) -> String {
        let hour = Calendar.current.component(.hour, from: date)
        let minute = Calendar.current.component(.minute, from: date)
        let minuteRounded = Int(minute / 30 * 30)

        return String(format: "%02d:%02d:00", hour, minuteRounded)
    }

    func addShift(days: [Int], startShift1: Date, endShift1: Date, startShift2:Date? = nil, endShift2:Date? = nil, _ dataHandler: @escaping (_ shift: Shift?, _ error: ResponseError? ) -> Void) -> () {

        let url = AppData.apiUrl + "RafJobShifts"
        guard AppData.userRole == .employer, AppData.token != nil, self.id != nil else {
            return
        }

        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer " + AppData.token!,
            ]

        let parameters: Parameters = [
            "JobKey": self.id!,
            "Days" : days.compactMap({String($0)}).joined(separator: ","),
            "StartTime1" : Job.dateToTimeString(date: startShift1),
            "EndTime1" : Job.dateToTimeString(date: endShift1),
            "StartTime2" : startShift2 != nil ? Job.dateToTimeString(date: startShift2!) : "",
            "EndTime2" : endShift2 != nil ? Job.dateToTimeString(date: endShift2!) : "",
            ]

        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Shift>) in
            switch response.result {

            case .success(let shift):
                guard shift.errors == nil else {
                    dataHandler(nil, shift.errors?.first)
                    return
                }

                dataHandler(shift, nil)

            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }

    }
    //TODO: test
    class func editShift(id: Int, days: [Int]? = nil, startShift1: Date? = nil, endShift1: Date? = nil, startShift2:Date? = nil, endShift2:Date? = nil, _ dataHandler: @escaping (_ shift: Shift?, _ error: ResponseError? ) -> Void) -> () {

        let url = AppData.apiUrl + "RafJobShifts"
        guard AppData.userRole == .employer, AppData.token != nil
        else {
            return
        }

        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer " + AppData.token!,
            ]

        let parameters: Parameters = [
            "Days" : days != nil ? days!.compactMap({String($0)}).joined(separator: ",") : "",
            "StartTime1" : startShift1 != nil ? Job.dateToTimeString(date: startShift1!) : "",
            "EndTime1" : endShift1 != nil ? Job.dateToTimeString(date: endShift1!) : "",
            "StartTime2" : startShift2 != nil ? Job.dateToTimeString(date: startShift2!) : "",
            "EndTime2" : endShift2 != nil ? Job.dateToTimeString(date: endShift2!) : "",
            ]

        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseObject { (response: DataResponse<Shift>) in
            switch response.result {

            case .success(let shift):
                guard shift.errors == nil else {
                    dataHandler(nil, shift.errors?.first)
                    return
                }

                dataHandler(shift, nil)

            case .failure(let error):
                if let netError = error as? URLError {
                    print("URLError: code =\(netError.code), \"\(netError.localizedDescription)\"")
                    dataHandler(nil, ResponseError(message: netError.localizedDescription, fields: nil))
                    return
                } else {
                    print("Can't map to anything, can't find error type, now just print out the raw response")
                }
            }
            }.responseString { (response:DataResponse<String>) in
                if let rawString = response.result.value {
                    switch response.response!.statusCode {
                    case 300...500 :
                        print(response.response!.statusCode)
                        dataHandler(nil, ResponseError(message: rawString,
                                                       fields: [FieldError(field: "statusCode",
                                                                           message: "\(response.response!.statusCode)")]
                        ))
                    case 200:
                        if rawString == "" {
                            print("Empty String: No news is good news!")
                        }
                    default:
                        print("Got the String response, but to little use.")
                        print(rawString.count, rawString)
                        print("Am I lost??")
                        print(response.response!.statusCode)
                    }
                }
        }

    }


}







