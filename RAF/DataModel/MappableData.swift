//
//  MappableData.swift
//  FoodCenter_DataLayer
//
//  Created by Hadi Sharghi on 4/16/18.
//  Copyright © 2018 SoftWorks. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

extension String {
    func toDate() -> Date {
        var str = self
        str.removeLast(5)
        str.removeFirst(6)
        return Date(timeIntervalSince1970: Double(str)!)
    }
}

extension Date {
    func toEpochMilliSecString() -> String {
        return "\(Int64(self.timeIntervalSince1970 * 1000))"
    }
}

// base Mappable data, with error property
class MappableData: Mappable {

    var errors: [ResponseError]?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        errors <- map["errors"]
    }

}

class ServerResponse: MappableData {
    var message: String?

    override func mapping(map: Map) {
        message <- map["Response"]
    }
}

// server response errors
class FieldError: Mappable {
    var field: String?
    var message: String?
    
    init(field: String?, message: String?) {
        self.field = field
        self.message = message
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        field <- map["field"]
        message <- map["message"]
    }
}

class ResponseError: Mappable {
    var message: String?
    var fields: [FieldError] = []
    
    
    init(message: String?, fields: [FieldError]?) {
        self.message = message
        self.fields = (fields ?? [])
    }
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        fields <- map["fields"]
    }
}


// for debugging purpose, print all properties of mappable object
extension Mappable {
    func description () -> Void {
        let reflected = Mirror(reflecting: self)
        var members = [String: String]()
        for index in reflected.children.indices {
            let child = reflected.children[index]
            members[child.label!] = child.value as? String
        }
        print(members.debugDescription)
    }
}


