//
//  StartViewController.swift
//  RAF
//
//  Created by Hadi Sharghi on 3/31/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    
    @IBOutlet weak var employerButton: UIButton!
    @IBOutlet weak var employeeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        employeeButton.rounded()
        employerButton.rounded()
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
       AppData.addGradientLayer(to: self.view)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
