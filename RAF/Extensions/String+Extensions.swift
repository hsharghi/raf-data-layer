    //
//  String+Extensions.swift
//  RAF
//
//  Created by Hadi Sharghi on 7/11/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import Foundation

    
    
    extension String {
        func toNumbers() -> Array<Int> {
            return self
                .components(separatedBy: ",")
                .compactMap {
                    Int($0.trimmingCharacters(in: .whitespaces))
            }
        }
    }

