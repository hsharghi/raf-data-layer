//
//  UIButton+Extensions.swift
//  RAF
//
//  Created by Hadi Sharghi on 3/31/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import UIKit

extension UIButton {
    func flip(withImage: Bool = false) {
        self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        self.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        if withImage {
            self.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
    }
    
    func rounded(color: UIColor? = nil, width: CGFloat = 1) {
        self.layer.cornerRadius = self.bounds.height / 2
        var borderColor = color
        if color == nil {
            borderColor = self.titleLabel?.textColor
        }
        self.layer.borderColor = borderColor?.cgColor
        self.layer.borderWidth = width
    }
}


extension UIView {
    func createGradientLayer(color1: UIColor, color2: UIColor, startPoint: CGPoint = CGPoint(x: 0,y: 0), endPoint: CGPoint = CGPoint(x:1.0,y:1.0)) {
    var gradientLayer: CAGradientLayer!
    
    gradientLayer = CAGradientLayer()
    
    gradientLayer.frame = self.bounds
    
    gradientLayer.colors = [color1.cgColor, color2.cgColor]
    
    
    gradientLayer.startPoint = startPoint
    gradientLayer.endPoint = endPoint
    gradientLayer.zPosition = -1
    self.layer.addSublayer(gradientLayer)
}

}
