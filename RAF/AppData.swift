
//
//  File.swift
//  RAF
//
//  Created by Hadi Sharghi on 3/31/1397 .
//  Copyright © 1397 Hadi Sharghi. All rights reserved.
//

import Foundation
import UIKit
//import Validator

class AppData {
    static let apiUrl = "http://raf.adad.ws/api/"
    static let webUrl = "http://raf.adad.ws/"

    enum UserType: Int {
        case employee = 3
        case employer = 4
    }
    
    
    private static let imeiKey = "imei"
    private static let tokenKey = "token"
    private static let roleKey = "userRole"
    private static let fcmTokenKey = "fcmToken"
    private static let tagsDataKey = "tagsData"
    private static let registerDataKey = "registerDataKey"
    private static let userIdKey = "userId"
    private static let cityKey = "city"
    private static let currentFilterKey = "currentFilter"
    private static let deviceRegisteredKey = "deviceRegistered"
    private static let itemsCountKey = "cartItemsCount"
    private static let cartDescriptionKey = "cartDescriptionKey"
    private static var allSideMenuItems: [String: Any]?

    
    static let BackgroundGradientDarkColor = UIColor(hexString: "5db483")
    static let BackgroundGradientLightColor = UIColor(hexString: "c2e1ba")
    static let greenColor = UIColor(red: 139 / 255, green: 194 / 255, blue: 74 / 255, alpha: 1)
    static let veryLightGreenColor = UIColor(red: 220 / 255, green: 240 / 255, blue: 170 / 255, alpha: 1)
    static let lightGreenColor = UIColor(red: 163 / 255, green: 187 / 255, blue: 44 / 255, alpha: 1)
    static let darkGreenColor = UIColor(red: 50 / 255, green: 180 / 255, blue: 115 / 255, alpha: 1)
    static let orangeColor = UIColor(red: 248 / 255, green: 162 / 255, blue: 39 / 255, alpha: 1)
    static let veryLightOrangeColor = UIColor(red: 255 / 255, green: 200 / 255, blue: 115 / 255, alpha: 1)
    static let darkRedColor = UIColor(red: 157 / 255, green: 36 / 255, blue: 33 / 255, alpha: 1)
    static let darkPurpleColor = UIColor(red: 159 / 255, green: 25 / 255, blue: 22 / 255, alpha: 1)
    static let veryLightGrayGreenColor = UIColor(red: 215 / 255, green: 219 / 255, blue: 205 / 255, alpha: 1)
    
    static let imageSizeThumb: UInt = 150
    static let imageSizeSmall: UInt = 310
    static let imageSizeMedium: UInt = 500
    static let imageSizeLarge: UInt = 1024
    
//    static let emailValidationRule = ValidationRulePattern(pattern: EmailValidationPattern.standard,
//                                                           error: ValidationError(message: ValidationMessages.emailInvalid))
//    static let companyNameValidationRule = ValidationRuleLength(min: 3,
//                                                                error: ValidationError(message: ValidationMessages.companyNameInvalid))
//    static let addressValidationRule = ValidationRuleLength(min: 3,
//                                                            error: ValidationError(message: ValidationMessages.addressInvalid))
//    static let mobileValidationRule = ValidationRulePattern(pattern: NumberOnlyValidationPattern(),
//                                                            error: ValidationError(message: ValidationMessages.mobileNumberInvalid))
//    static let passwordLengthValidationRule = ValidationRuleLength(min: 6, max: 12,
//                                                                   error: ValidationError(message: ValidationMessages.passwordLength))

    
    static func addGradientLayer(to view: UIView) {
        view.createGradientLayer(color1: AppData.BackgroundGradientDarkColor,
                                 color2: AppData.BackgroundGradientLightColor,
                                 startPoint: CGPoint(x: 0.2,y: 1.0),
                                 endPoint:  CGPoint(x: 0.8,y: 0.0))
    }
    
    

    static var appDelegate: AppDelegate? {
        get {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                return delegate
            }

            return nil
        }
    }



    static var token: String? {
        get {
            return UserDefaults.standard.string(forKey: tokenKey)
        }
        set(newToken) {
            UserDefaults.standard.set(newToken, forKey: tokenKey)
        }
    }


    static var registerData: User.RegisterData? {
        get {
            if let data = UserDefaults.standard.value(forKey:registerDataKey) as? Data {
                if let registerData = try? PropertyListDecoder().decode(User.RegisterData.self, from: data) {
                    return registerData
                }
            }

            return nil
        }
        set(newRegisterData) {
            if let data = try? PropertyListEncoder().encode(newRegisterData) {
                UserDefaults.standard.set(data, forKey: registerDataKey)
                UserDefaults.standard.synchronize()
            }
        }
    }



    static var userRole: User.Role? {
        get {
            let role = UserDefaults.standard.integer(forKey: roleKey)
            if role > 0 {
                return User.Role(rawValue: role)
            }

            return nil
        }
        set(newRoleId) {
            UserDefaults.standard.set(newRoleId?.rawValue, forKey: roleKey)
            UserDefaults.standard.synchronize()
        }
    }


    static func showAlert(_ title: String, message : String) -> Void {
        AppData.appDelegate?.showAlert(title, message: message)
    }
    
    
    
    




}




